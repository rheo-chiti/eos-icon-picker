const fs = require('fs');
const zipFolder = require('zip-folder');

class CustomFont {
  constructor(icons, ){
    this.timestamp = timestamp;
    this.icons = icons;
  }

  gruntCdIcons(){
    var command = '';
    try {
      for (var i = 0; i < this.icons.length; i++) {
        command += (`--extended_src=svg/${this.icons[i]}.svg `);
      }
      console.log('✅Generated Icons set')
      return command;
    }
    catch (err) {
      console.log('⛔️Some error occurred while generating Icons set command');
      return '';
    }
  }

  commandFinal(){
    var gruntCdIcons = this.gruntCdIcons();
    return `grunt --dist=${this.timestamp} ${gruntCdIcons}`;
  }

  generateFiles(){
    //Generating icons_config.json file
    fs.writeFile(`${__dirname}/../temp/dist_${this.timestamp}/icons_config.json`, JSON.stringify({"icons": this.icons, "exportAs": "font"}), function(err, result) {
      if(err) {
        console.log('⛔️Some error occurred while generating icons_config.json: ', err);
      }
    });

     //making zip file of dist
    zipFolder(`${__dirname}/../temp/dist_${this.timestamp}`, `${__dirname}/../temp/dist_${this.timestamp}.zip`, function(err) {
      if(err) {
        console.log('⛔️Some error occurred while generating zip file:', err);
      }
    });
  }
}

module.exports = CustomFont;
